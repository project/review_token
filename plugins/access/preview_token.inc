<?php

/**
 * @file
 * Plugin to provide access control based upon a matching preview token.
 */

$plugin = array(
  'title' => t("Node: matching preview token"),
  'description' => t('Control access by the nodes published status.'),
  'callback' => 'ctools_preview_token_ctools_access_check',
  'summary' => 'ctools_preview_token_ctools_access_summary',
  'required context' => array(
    new ctools_context_required(t('Node'), 'node'),
    new ctools_context_required(t('Token'), 'string'),
  ),
);

/**
 * Check for access by comparing node's token with supplied token.
 */
function ctools_preview_token_ctools_access_check($conf, $context) {
  $node = $context[0]->data;
  // Supplied preview token (string).
  $token = $context[1]->data;
  if (!empty($node) && !empty($token)) {
    if (isset($node->preview_token)) {
      return $node->preview_token == $token;
    }
    else {
      return FALSE;
    }
  }
  return FALSE;
}

/**
 * Provide a summary description.
 */
function ctools_preview_token_ctools_access_summary($conf, $context) {
  return t('Returns true if a matching preview token string is provided.');
}
