<?php

/**
 * @file
 * Plugin to provide an relationship handler for a node preview token.
 */

$plugin = array(
  'title' => t('Preview token'),
  'keyword' => 'preview_token',
  'description' => t('Adds a preview token string from a node context.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'context' => 'ctools_node_preview_token_context',
);

/**
 * Return a new string context based on an existing node context.
 */
function ctools_node_preview_token_context($context, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_string('');
  }
  if (isset($context->data->preview_token)) {
    return ctools_context_create('string', $context->data->preview_token);
  }
  else {
    return ctools_context_create_empty('node');
  }
}
