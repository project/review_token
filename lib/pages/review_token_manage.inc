<?php
/**
 * @file
 * Content publish review URL management tab.
 */

use Drupal\review_token\NodeToken\PreviewToken;

/**
 * Content review admin page.
 *
 * Displays current preview URL.
 * Functions include generating and expiring content review URLs.
 *
 * @param (object) $node
 *   Node being managed.
 *
 * @return array
 *   Renderable array.
 */
function review_token_manage($node) {
  $output = array();
  $token = new PreviewToken($node);

  $no_url_txt = '<p>No active token has been generated for this node.</p>';
  $markup = '';

  if (!empty($token)) {
    $token_url = $token->url();
    $markup = l($token_url, $token_url);
  }
  else {
    $markup = $no_url_txt;
  }
  $output['token'] = array(
    '#type' => 'markup',
    '#markup' => '<div class="preview-link">' . $markup . '</div>',
  );

  $form = drupal_get_form('review_token_admin_form', $token);
  $output['actions'] = $form;

  return $output;
}


/**
 * Content publish review action buttons.
 *
 * @return array
 *   Form array with URL manage function buttons.
 */
function review_token_admin_form() {
  $form = array();
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['regenerate'] = array(
    '#type' => 'submit',
    '#value' => t('Regenerate URL'),
  );
  $form['actions']['expire'] = array(
    '#type' => 'submit',
    '#value' => t('Expire URL'),
  );

  return $form;
}


/**
 * Submit handler for URL management action buttons.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 */
function review_token_admin_form_submit($form, &$form_state) {
  $token = $form_state['build_info']['args'][0];
  $nid = $token->getNode()->nid;
  $op = $form_state['values']['op'];

  switch ($op) {
    case 'Regenerate URL':
      $token->expireToken();
      drupal_set_message('New publish review URL generated.');
      drupal_goto('node/' . $nid . '/review-token');
      break;

    case 'Expire URL':
      $token->expireToken();
      drupal_set_message('Publish review URL expired.');
      drupal_goto('node/' . $nid);
      break;
  }

}
