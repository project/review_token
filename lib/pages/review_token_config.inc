<?php

/**
 * @file
 * Content review configuration page.
 */

/**
 * Global config settings form.
 *
 * @return array
 *   System settings form.
 */
function review_token_config_form() {
  $form = array();
  $form['review_token_content_max_token'] = array(
    '#title' => 'Maximum token lifetime (hours)',
    '#type' => 'textfield',
    '#length' => 32,
    '#default_value' => variable_get('review_token_content_max_token', 24),
  );

  return system_settings_form($form);
}
