<?php

/**
 * @file
 * Per node preview token class.
 */

namespace Drupal\review_token\NodeToken;

/**
 * Preview token for a node.
 *
 * A preview token can be created for any node, its data is persisted to the
 * database and it can be expired.
 */
class PreviewToken {

  protected $node    = NULL;
  protected $token   = '';
  // Unix timestamp.
  protected $expires = 0;

  /**
   * Constructor.
   *
   * @param (object) $node
   *   Node that this token relates to.
   */
  public function __construct($node) {
    if (!is_object($node) || empty($node->nid)) {
      throw new \InvalidArgumentException('Node malformed or not an object');
    }
    $this->node = $node;
    // Generate (if new), save and persist token and expiration timestamp.
    $this->initToken();
  }

  /**
   * Accessor for token's node.
   *
   * @return (object)
   *   Node.
   */
  public function getNode() {
    return $this->node;
  }

  /**
   * Accessor for token's string.
   *
   * @return string
   *   Preview token.
   */
  public function getToken() {
    return $this->token;
  }

  /**
   * Expire token, and remove it from database.
   */
  public function expireToken() {
    // Flush cached page.
    $cache = _cache_get_object('cache_page');
    if (!empty($cache)) {
      // Destroy the page cache of the token preview.
      $url = $this->url();
      $cache->clear($url);
    }
    db_delete('node_preview_tokens')
      ->condition('nid', $this->getNode()->nid)
      ->execute();
    $this->token = '';
    $this->expires = 0;
  }

  /**
   * Preview URL based on token string (absolute link).
   *
   * @return string
   *   Preview URL.
   */
  public function url() {
    $token_url = url(
      'node/' . $this->getNode()->nid . '/preview/' . $this->getToken(),
      array('absolute' => TRUE)
    );
    return $token_url;
  }

  /**
   * Bulk clean up tokens whose associated node no longer exists.
   *
   * Cleans up tokens who escaped hook_node_delete().
   */
  public static function cleanUp() {
    // Get nids on preview tokens table which no longer exist.
    $query = db_select('node_preview_tokens', 't');
    $query->leftJoin('node', 'n', 't.nid = n.nid');
    $query
      ->fields('t', array('nid'))
      ->isNull('n.nid')
      ->distinct();

    /** @var int[] $nids */
    $nids = $query->execute()->fetchCol();

    if (count($nids)) {
      $deleted = db_delete('node_preview_tokens')
        ->condition('nid', $nids, 'IN')
        ->execute();

      watchdog('review_token', '%deleted %tokens since their associated node disappeared.',
        array(
          '%deleted' => $deleted,
          '%tokens' => format_plural($deleted, 'token', 'tokens'),
        ),
        WATCHDOG_INFO
      );
    }
  }


  /**
   * Initialise token data and persist.
   *
   * If token exists for this $node then use that, otherwise generate new token
   * string and expiration timestamp.
   */
  protected function initToken() {
    $token = $this->loadToken();
    if (empty($token)) {
      $token_string  = bin2hex(openssl_random_pseudo_bytes(32));
      $token_expires = time() + (variable_get('review_token_content_max_token', 24) * 60 * 60);
      $this->writeToken($token_string, $token_expires);
    }
    else {
      $this->token   = $token['token'];
      $this->expires = $token['expires'];
    }
  }

  /**
   * Check database for token data for this node.
   *
   * Nodes can only have one token string and expiration timestamp active at one
   * time.
   *
   * @return array
   *   Assoc array of nid, token string and expire time.
   */
  protected function loadToken() {
    $nid = $this->getNode()->nid;
    $result = db_select('node_preview_tokens', 't')
      ->fields('t')
      ->condition('nid', $nid)
      ->execute()->fetchAssoc();
    if (!empty($result['nid'])) {
      return $result;
    }
    else {
      return array();
    }
  }

  /**
   * Set token data and persist to database.
   *
   * @param string $token
   *   Token string (hex).
   * @param int $expires
   *   Expiration (Unix timestamp).
   */
  protected function writeToken($token, $expires) {
    db_delete('node_preview_tokens')
      ->condition('nid', $this->getNode()->nid)
      ->execute();

    $record = new \stdClass();
    $record->nid     = $this->getNode()->nid;
    $record->token   = $token;
    $record->expires = $expires;
    drupal_write_record('node_preview_tokens', $record);

    $this->token   = $token;
    $this->expires = $expires;
    // Clear the static loading cache.
    entity_get_controller('node')->resetCache(array($this->getNode()->nid));
  }
}
